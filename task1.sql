USE database_design_drills;

CREATE TABLE books (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  branch INTEGER REFERENCES branches(id),
  isbn VARCHAR(13),
  title TEXT,
  author INTEGER REFERENCES authors(id),
  publisher INTEGER REFERENCES publishers(id),
  noOfCopies INTEGER
);

CREATE TABLE publishers (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  publisherName TEXT,
  publisherAddress TEXT
);

CREATE TABLE authors (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  firstName TEXT,
  lastName TEXT,
  adress TEXT
);

CREATE TABLE branches (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  location TEXT
)