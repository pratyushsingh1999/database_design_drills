USE database_design_drills;

CREATE TABLE clients (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  clientName TEXT,
  locationId INTEGER REFERENCES locations(id),
  contractId INTEGER REFERENCES contracts(id),
  managerId INTEGER REFERENCES manager(id)
);
CREATE TABLE manager (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  managerName TEXT,
  locationId INTEGER REFERENCES locations(id)
);
CREATE TABLE contracts (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  projectName TEXT,
  estimatedCost INTEGER,
  completionDate DATE,
  staffs INTEGER REFERENCES contractAndStaffs(contractId)
);
CREATE TABLE locations (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  address TEXT
);
CREATE TABLE staffs (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  staffName TEXT,
  locationId INTEGER REFERENCES locations(id)
);
CREATE TABLE contractAndStaffs (
  contractId INTEGER,
  staffId INTEGER
);