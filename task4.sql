USE database_design_drills;

CREATE TABLE doctors (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  doctorName TEXT,
  secretaryId INTEGER REFERENCES secretarys(id),
  address INTEGER REFERENCES locations(id)
);
CREATE TABLE patients (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  patientName TEXT,
  dob DATE,
  address INTEGER REFERENCES locations(id),
  prescription INTEGER REFERENCES prescriptions(id)
);
CREATE TABLE patientsOfDoctors (
  doctorId INTEGER REFERENCES doctors(id),
  patientId INTEGER REFERENCES patients(id)
);
CREATE TABLE secretarys (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  secretaryName TEXT,
  specialization TEXT
);



CREATE TABLE prescriptions (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  issuedDate DATE,
  doctorId INTEGER REFERENCES doctors(id),
  dosage Text
);
CREATE TABLE prescriptionAndDrug (
  perscriptionId INTEGER REFERENCES perscriptions(id),
  drugId INTEGER REFERENCES drugs(id)
);
CREATE TABLE drugs (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  drugName TEXT
);
CREATE TABLE locations (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  street TEXT,
  pin TEXT
);


